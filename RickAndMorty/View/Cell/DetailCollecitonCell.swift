//
//  DetailCollecitonCell.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 05.07.2024.
//

import UIKit

class DetailCollecitonCell: UICollectionViewCell {

    static let cellIdentifier = "cellID"
    
    let bellImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(systemName: "bell")
        return iv
    }()
    
    let infoLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 20, weight: .regular)
        label.text = "None"
        
        return label
    }()
    
    let infoButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .systemGray6
        button.titleLabel?.font = .boldSystemFont(ofSize: 17)
     
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        contentView.addSubview(bellImageView)
        contentView.addSubview(infoLabel)
        contentView.addSubview(infoButton)
    }
    
    func setupConstraints() {
        bellImageView.snp.makeConstraints {
            $0.leading.equalToSuperview().inset(25)
            $0.top.equalToSuperview()
            $0.size.equalTo(25)
        }
        
        infoLabel.snp.makeConstraints {
            $0.top.trailing.equalToSuperview()
            $0.leading.equalTo(bellImageView.snp.trailing).offset(15)
        }
        
        infoButton.snp.makeConstraints {
            $0.height.equalTo(35)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func configure(info: DetailCharacter?) {
        guard let info = info else { return}
        infoButton.setTitle(info.infoType, for: .normal)
        infoButton.setTitleColor(info.color, for: .normal)
        bellImageView.tintColor = info.color
        
        if info.info != "" {
            infoLabel.text = info.info
        }
    }
}
