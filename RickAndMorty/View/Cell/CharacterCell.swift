//
//  CharacterCell.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 04.07.2024.
//

import UIKit
import Kingfisher

class CharacterCell: UICollectionViewCell {
    
    var data: Character! {
        didSet {
            nameLabel.text = data.name
            statusLabel.text = "Status: \(data.status)"
            
            if let imageURL = URL(string: data.image) {
                imageView.kf.setImage(with: imageURL)
            }
        }
    }
    
    static let cellIdentifier = "cellID"
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(systemName: "phone.fill")
        return iv
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 14)
        
        return label
    }()
    
    let statusLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .regular)
        label.textColor = .darkGray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        contentView.addSubview(imageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(statusLabel)
    }
    
    func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalToSuperview()
            $0.leading.trailing.equalToSuperview()
        }
        
        nameLabel.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(5)
            $0.leading.equalToSuperview().inset(5)
        }
        
        statusLabel.snp.makeConstraints {
            $0.top.equalTo(nameLabel.snp.bottom).offset(5)
            $0.leading.equalToSuperview().inset(5)
            $0.bottom.equalToSuperview()
        }
    }
    
}
