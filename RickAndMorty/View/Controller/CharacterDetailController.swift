//
//  CharacterDetailController.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 04.07.2024.
//

import UIKit

class CharacterDetailController: UIViewController {

    var characterData: Character?
    let characterViewModel = CharacterViewModel.shared

    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()

    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(DetailCollecitonCell.self, forCellWithReuseIdentifier: DetailCollecitonCell.cellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupConstraints()
        view.backgroundColor = .white
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.largeTitleDisplayMode = .never
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationItem.largeTitleDisplayMode = .always
    }

    private func setupView() {
        view.addSubview(imageView)
        view.addSubview(collectionView)
    }

    private func setupConstraints() {
        imageView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide)
            $0.leading.trailing.equalToSuperview()
            $0.height.equalTo(view.snp.width)
        }

        collectionView.snp.makeConstraints {
            $0.top.equalTo(imageView.snp.bottom).offset(30)
            $0.leading.trailing.equalToSuperview()
            $0.bottom.equalToSuperview()
        }
    }

    func getCharacterData(id: Int) {
        characterViewModel.fetchCharacter(id: id) { [weak self] character in
            guard let self = self, let character = character else { return }

            DispatchQueue.main.async {
                self.title = character.name
                if let imageURL = URL(string: character.image) {
                    self.imageView.kf.setImage(with: imageURL)
                }
                self.characterData = character
                self.collectionView.reloadData()
            }
        }
    }
}

extension CharacterDetailController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailCollecitonCell.cellIdentifier, for: indexPath) as! DetailCollecitonCell
        cell.configure(info: whatToShow(index: indexPath.row))
        
        return cell
    }
    
    func whatToShow(index: Int) -> DetailCharacter? {
        guard let character = characterData else { return nil }
        switch index {
        case 0:
            return DetailCharacter(color: .systemBlue, infoType: "STATUS", info: character.status)
        case 1:
            return DetailCharacter(color: .systemRed, infoType: "GENDER", info: character.gender)
        case 2:
            return DetailCharacter(color: .systemPurple, infoType: "TYPE", info: character.type)
        default:
            return DetailCharacter(color: .systemGreen, infoType: "SPECIES", info: character.species)
        }
    }
}

extension CharacterDetailController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        
        return CGSize(width: (collectionViewWidth / 2) - 5, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 40
    }
}
