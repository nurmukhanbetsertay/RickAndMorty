//
//  CharachterViewController.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 04.07.2024.
//

import UIKit

class CharactersController: UICollectionViewController {
    
    fileprivate var cellID = "cellID"
    let characterViewModel = CharacterViewModel.shared
    
    override func viewDidLoad() {
        collectionView.register(CharacterCell.self, forCellWithReuseIdentifier: cellID)
        
        characterViewModel.fetchCharacters { _ in
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension CharactersController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return characterViewModel.characters.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! CharacterCell
        cell.data = characterViewModel.characters[indexPath.row]
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = CharacterDetailController()
        vc.getCharacterData(id: characterViewModel.characters[indexPath.row].id)
        
        navigationController?.pushViewController(vc, animated: true)
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}


extension CharactersController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width
        
        return .init(width: (collectionViewWidth / 2) - 25, height: 250)
    }
    
    var insets: UIEdgeInsets {
        return .init(top: 12, left: 16, bottom: 12, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return insets
    }
}
