//
//  UpdateNowController.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 05.07.2024.
//

import UIKit

class UpdateNowController: UIViewController {

    private let updateNowButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "exclamationmark.triangle"), for: .normal)
        button.setTitle("UPDATE NOW", for: .normal)
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 10
        
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupView()
        setupConstraints()
    }
    
    func setupView() {
        view.addSubview(updateNowButton)
    }
    
    func setupConstraints() {
        updateNowButton.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.height.equalTo(50)
            $0.bottom.equalToSuperview().inset(50)
        }
    }
}
