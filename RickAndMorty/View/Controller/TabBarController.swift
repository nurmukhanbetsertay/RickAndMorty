//
//  TabBarController.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 04.07.2024.
//

import UIKit
import SnapKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        viewControllers = [
            createNavController(vc: CharactersController(), title: "Characters", imageName: "person"),
            createNavController(vc: UIViewController(), title: "Locations", imageName: "globe"),
            createNavController(vc: UIViewController(), title: "Episods", imageName: "tv"),
            createNavController(vc: UIViewController(), title: "Settings", imageName: "gear"),
        ]
    }
    
    fileprivate func createNavController(vc: UIViewController, title: String, imageName: String) -> UIViewController {
        vc.title = title
        vc.view.backgroundColor = .white
        
        let navController = UINavigationController(rootViewController: vc)
        navController.tabBarItem.title = title
        navController.tabBarItem.image = UIImage(systemName: imageName)
        navController.navigationBar.prefersLargeTitles = true
        
        return navController
    }
}
