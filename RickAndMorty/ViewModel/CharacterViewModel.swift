//
//  CharacterViewModel.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 05.07.2024.
//

import Foundation

class CharacterViewModel {
    static let shared = CharacterViewModel()
    
    private let apiService = APIService.shared
    var characters: [Character] = []
    var character: Character?
    
    func fetchCharacters(completion: @escaping ([Character]?) -> Void) {
        apiService.fetchCharacters { characters in
            if let characters = characters {
                self.characters = characters
            }
            completion(characters)
        }
    }
    
    func fetchCharacter(id: Int, completion: @escaping (Character?) -> Void) {
        apiService.fetchCharacter(id: id) { character in
            if let character = character {
                self.character = character
            }
            completion(character)
        }
    }
}
