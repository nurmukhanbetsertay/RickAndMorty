//
//  SceneDelegate.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 04.07.2024.
//

import UIKit
import FirebaseRemoteConfig

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    var remoteConfig: RemoteConfig!
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        remoteConfig.setDefaults(["needForceUpdate": NSNumber(value: false)])
        
        fetchRemoteConfig()
        
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            self.window = window
            self.window?.backgroundColor = .white
        }
    }
    
    func fetchRemoteConfig() {
        remoteConfig.fetch { [weak self] (status, error) in
            if status == .success {
                self?.remoteConfig.activate { (changed, error) in
                    self?.checkForceUpdate()
                }
            } else {
                self?.checkForceUpdate()
            }
        }
    }
    
    func checkForceUpdate() {
        let needForceUpdate = remoteConfig["needForceUpdate"].boolValue
        
        DispatchQueue.main.async {
            if needForceUpdate {
                self.showForceUpdateScreen()
            } else {
                self.continueAppFunctionality()
            }
        }
    }
    
    func showForceUpdateScreen() {
        window?.rootViewController = UpdateNowController()
        window?.makeKeyAndVisible()
    }
    
    func continueAppFunctionality() {
        window?.rootViewController = TabBarController()
        window?.makeKeyAndVisible()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }
    
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
    
    
}

