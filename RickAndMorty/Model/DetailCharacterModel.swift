//
//  DetailCharacterModel.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 05.07.2024.
//

import Foundation
import UIKit

struct DetailCharacter {
    let color: UIColor
    let infoType: String
    let info: String
}
