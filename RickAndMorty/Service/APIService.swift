//
//  APIService.swift
//  RickAndMorty
//
//  Created by Nurmukhanbet Sertay on 04.07.2024.
//

import Foundation

class APIService {
    static let shared = APIService()
    
    private init() {}
    
    func fetchCharacters(completion: @escaping ([Character]?) -> Void) {
        guard let url = URL(string: "https://rickandmortyapi.com/api/character") else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            do {
                let response = try JSONDecoder().decode(APIResponse.self, from: data)
                completion(response.results)
            } catch {
                completion(nil)
            }
        }.resume()
    }
    
    
    func fetchCharacter(id: Int, completion: @escaping (Character?) -> Void) {
        guard let url = URL(string: "https://rickandmortyapi.com/api/character/\(id)") else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            guard let data = data, error == nil else {
                completion(nil)
                return
            }
            
            do {
                let response = try JSONDecoder().decode(Character.self, from: data)
                completion(response)
            } catch {
                completion(nil)
            }
        }.resume()
    }
}
